package ru.omsu.imit;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import ru.omsu.imit.index.MessageIndexer;
import ru.omsu.imit.search.Searcher;
import ru.omsu.imit.utlis.MessageToDocument;

import java.io.IOException;
import java.util.Scanner;

public class UserRequestSearch {
    private static final String teaserTitle = "Название документа";
    private static final String teaserBody = "Тело документа";

    public static void main(String[] args) throws IOException, ParseException {
        final Document teaserDoc = MessageToDocument.createWith(teaserTitle, teaserBody);
        final MessageIndexer indexer = new MessageIndexer("/tmp/data_index");
        indexer.index(true, teaserDoc);

        final Searcher search = new Searcher(indexer.readIndex());

        final Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.print("Введите запрос:\t");
        final String toSearch = reader.nextLine(); // Scans the next token

        search.fuzzySearch(toSearch);
    }
}
