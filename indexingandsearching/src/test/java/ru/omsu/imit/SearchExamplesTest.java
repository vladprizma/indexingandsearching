package ru.omsu.imit;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.After;
import org.junit.Test;
import ru.omsu.imit.index.MessageIndexer;
import ru.omsu.imit.search.Searcher;
import ru.omsu.imit.utlis.Constants;
import ru.omsu.imit.utlis.Reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class SearchExamplesTest {
    private final Random rnd = new Random(); // to generate safe name for index folder. After tests we removing folders
    private final MessageIndexer indexer = new MessageIndexer(Constants.TMP_DIR + "/search_test" + rnd.nextInt());
    final ClassLoader classLoader = getClass().getClassLoader();
    final File file = new File(classLoader.getResource("data.json").getFile());
    final List<Document> documents;

    public SearchExamplesTest() throws FileNotFoundException {
        documents = Reader.readDocumentsFromFile(file);
    }

    @Test
    public void testSearch() throws IOException, ParseException {
        indexer.index(true, documents); // create index

        final Searcher searchWith = new Searcher(indexer.readIndex());
        searchWith.searchInBody("корреспондент");
    }

    @Test
    public void testSearchWithMistake() throws IOException, ParseException {
        indexer.index(true, documents); // create index

        final Searcher searchWith = new Searcher(indexer.readIndex());
        searchWith.searchInBody("кореспондент");
    }

    @Test
    public void testSearchSuccess() throws IOException, ParseException {
        indexer.index(true, documents); // create index

        final Searcher searchWith = new Searcher(indexer.readIndex());
        searchWith.searchInBody("ДВД");
    }

    @Test
    public void fuzzySearchWithMistake() throws IOException, ParseException {
        indexer.index(true, documents); // create index

        final Searcher searchWith = new Searcher(indexer.readIndex());
        searchWith.fuzzySearch("кореспондент");
    }

    @Test
    public void fuzzySearch() throws IOException, ParseException {
        indexer.index(true, documents); // create index

        final Searcher searchWith = new Searcher(indexer.readIndex());
        searchWith.fuzzySearch("дорога");
    }

    @After
    public void removeIndexes() {
        FileUtils.deleteQuietly(new File(indexer.getPathToIndexFolder())); // remove indexes
    }
}
