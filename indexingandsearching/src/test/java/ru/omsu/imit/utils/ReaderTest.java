package ru.omsu.imit.utils;

import org.apache.lucene.document.Document;
import org.junit.Test;
import ru.omsu.imit.utlis.Reader;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ReaderTest {

    // Path to directory should be fully on english
    @Test
    public void testReadDocumentsFromFile() throws IOException {
        final ClassLoader classLoader = getClass().getClassLoader();
        final File file = new File(classLoader.getResource("data.json").getFile());
        final List<Document> documents = Reader.readDocumentsFromFile(file);
        assertEquals("Should successfully read 17 documents", 17, documents.size());
    }
}
