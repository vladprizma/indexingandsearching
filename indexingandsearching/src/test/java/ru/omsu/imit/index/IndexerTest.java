package ru.omsu.imit.index;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.omsu.imit.utlis.Constants;
import ru.omsu.imit.utlis.MessageToDocument;

import java.io.File;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

// https://lucene.apache.org/core/9_0_0/demo/index.html

public class IndexerTest {
    private final Random rnd = new Random(); // to generate safe name for index folder. After tests we removing folders

    private final MessageIndexer indexer = new MessageIndexer(Constants.TMP_DIR + "/index_test" + rnd.nextInt());
    private final String body = "Это тестовое тело документа.";
    private final String title = "Это тестовый загаловок документа.";

    private Document document = null;

    @Test
    @Before
    public void testIndex(){
        document = MessageToDocument.createWith(title, body);
        assertNotNull("Created document should not be null", document);
        assertEquals(body, document.get("body"));
        assertEquals(title, document.get("title"));

    }

    @Test
    @After
    public void testReadIndex() throws Exception {
        indexer.index(true, document); // should create index without any errors

        final IndexReader indexReader = indexer.readIndex();
        assertNotNull("IndexReader should not be null", indexReader);

        FileUtils.deleteQuietly(new File(indexer.getPathToIndexFolder())); // remove indexes
    }
}
